<?php

class Test_Dynamicattribute_Model_Observer{
    public $dynamicValue = 'dynamic value';
    public $attributeName = 'custom_attribute';

    public function catalogProductLoadAfter($observer){
        $product = $observer->getEvent()->getProduct();
        $product->addData(array($this->attributeName => $this->dynamicValue));
    }
}